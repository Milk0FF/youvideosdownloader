## About YouVideosDownloader

That application allow you download videos from YouTube.

## Installation

You should change your .env file

-   DB_DATABASE=your_db_name
-   DB_USERNAME=your_user_name
-   DB_PASSWORD=your_user_password

You should running following commands

-   php artisan migrate
-   php artisan serve
