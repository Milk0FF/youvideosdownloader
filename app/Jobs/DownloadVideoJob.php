<?php

namespace App\Jobs;

use App\Events\NewVideoEvent;
use App\Models\Video;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\YoutubeDownloaderService;
use YoutubeDownloader\YoutubeDownloader as YoutubeDownloader;
use Exception;

class DownloadVideoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $video; 
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            // // $info = file_get_contents("https://youtube.com/get_video_info?video_id=en6ZUDjvUEs");
            // $yt = new YoutubeDownloader();

            // $registrationResponse = $yt->registerDevice();
            
            // // $playerResponse = $yt->playerRequest->fetch_player_info('en6ZUDjvUEs');
            // // $info = $playerResponse->serializeToJsonString();
            
            // logs()->info("Информация из видео $registrationResponse");
            // logs()->info("Видео скачалось!");
            // // $mytube = new YoutubeDownloaderService('https://www.youtube.com/watch?v=en6ZUDjvUEs');
            // // logs()->info("Путь до видео: {$mytube->get_downloads_dir()}");
            // // $mytube->download_video();
            
            $options = [
                '-f best',
                escapeshellarg('-o' . 'storage/downloads' . '/%(title)s.%(ext)s'),
            ];
            $videoUrl = 'https://www.youtube.com/watch?v=' . $this->video->video_id;
            $command = 'youtube-dl ' . implode(' ', $options) . ' ' . $videoUrl;
            exec($command, $response, $status);
            
            $videoPath = substr($response[1], 24);
            $this->video->status = 2;
            $this->video->video_url = $videoPath;
            $this->video->save();
            event(new NewVideoEvent($this->video->id, 2));

        }
        catch(Exception $e){
            $this->video->status = 3;
            $this->video->save();
            event( new NewVideoEvent($this->video->id, 3));
        }
    }
}
