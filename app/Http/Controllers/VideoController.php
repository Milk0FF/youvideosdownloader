<?php

namespace App\Http\Controllers;

use App\Events\NewVideoEvent;
use App\Http\Resources\VideoResource;
use App\Jobs\DownloadVideoJob;
use App\Models\Video;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use YoutubeDownloader\YoutubeDownloader;

class VideoController extends Controller
{
    private $videoStatuses = [
        0 => 'created',
        1 => 'pending',
        2 => 'finish',
        3 => 'error',
    ];

    public function downloadVideo(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'id' => 'required|integer',
        ]);
        $response = [
            'status' => 200,
            'data' => null,
        ];
        if($validator->fails()){
            $response['status'] = 400;
            $response['data'] = [
                'message' => 'Validation error',
                'errors' => $validator->errors(),
            ];
            return response()->json($response, 400);
        }
        $videoId = $data['id'];
        $video = Video::find($videoId);
        if(!$video){
            $response['status'] = 404;
            $response['data'] = [
                'message' => 'Video not found',
            ];
            return response()->json($response, 404);
        }
        $video->status = 1;
        $video->save();
        event(new NewVideoEvent($video->id, 1));
        DownloadVideoJob::dispatch($video);
    }
    public function getVideoInfo(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'url' => 'required|url',
        ]);
        $response = [
            'status' => 200,
            'data' => null,
        ];
        if($validator->fails()){
            $response['status'] = 400;
            $response['data'] = [
                'message' => 'Validation error',
                'errors' => $validator->errors(),
            ];
            return response()->json($response, 400);
        }
        $options = [
            '--get-title',
            '--get-description',
            '--get-thumbnail',
            '--get-id',
        ];
        $url = $data['url'];
        $videoInfo = null;
        $status = null;
        $command = 'youtube-dl ' . implode(' ', $options) . ' ' . $url;
        exec($command, $videoInfo, $status);
        
        if($status){
            $response['status'] = 400;
            $response['data'] = [
                'message' => 'Url incorrect',
            ];
            return response()->json($response, 400);
        }
        $video = Video::where('video_id', $videoInfo[1])->first();
        if($video){
            $response['status'] = 400;
            $response['data'] = [
                'message' => 'This video already exist',
            ];
            return response()->json($response, 400);
        }
        $video = Video::create([
            'title'                 => $videoInfo[0], 
            'description'           => $videoInfo[3],
            'thumbnail_url'         => $videoInfo[2],
            'video_id'              => $videoInfo[1],
            'status'                => 0,
        ]);
        $response['data'] = VideoResource::make($video);
        return response()->json($response);
    }
    public function getAllVideos()
    {
        $response = [
            'status' => 200,
            'data' => VideoResource::collection(Video::all()),
        ];
        return response()->json($response, 200); 
    }
    public function deleteVideo(int $id)
    {
        $response = [
            'status' => 200,
            'data' => [
                'message' => 'Video succesfully deleted',
            ],
        ];
        $video = Video::find($id);
        if(!$video){
            $response['status'] = 404;
            $response['data'] = [
                'message' => 'Video not found',
            ];
            return response()->json($response, 404);
        }
        $video->delete();
        unlink(public_path() . '/' . $video->video_url);
        return response()->json($response, 200); 
    }
}
