<?php

use App\Http\Controllers\VideoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/videos', [VideoController::class, 'getAllVideos']);
Route::get('/video-info', [VideoController::class, 'getVideoInfo']);
Route::post('/add-video', [VideoController::class, 'downloadVideo']);
Route::delete('/videos/{id}', [VideoController::class, 'deleteVideo']);
